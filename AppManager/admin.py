from django.contrib import admin
from AppManager.models import App, Installation


admin.site.register(App)
admin.site.register(Installation)

