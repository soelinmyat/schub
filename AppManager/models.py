from django.db import models
from django_roa import Model as ROAModel

from AppManager import routines
from SEHub.settings import SECENTRAL_IP


class CommonROAModel(ROAModel):

    class Meta:
        abstract = True

    @classmethod
    def get_resource_url_list(cls):
        temp_url = unicode(SECENTRAL_IP + "apps/api/{base_name}/")
        temp_url = temp_url.format(
            base_name=cls.api_base_name,
        )
        print temp_url
        return temp_url

    def get_resource_url_count(self):
        return self.get_resource_url_list()


class App(CommonROAModel):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    package_name = models.CharField(max_length=32)
    desc = models.TextField()
    prefix = models.CharField(max_length=64)
    version = models.CharField(max_length=16)

    api_base_name = 'apps'

    @classmethod
    def serializer(cls):
        from .serializers import AppSerializer
        return AppSerializer

    def __unicode__(self):
        return str(self.package_name)


class Installation(models.Model):
    app = models.ForeignKey(App)

    def save(self, *args, **kwargs):
        routines.install(self.app.package_name, self.app.name, self.app.prefix)
        super(Installation, self).save(*args, **kwargs)

    def delete(self):
        print self.app.prefix
        routines.uninstall(self.app.package_name, self.app.name, self.app.prefix)
        super(Installation, self).delete()
