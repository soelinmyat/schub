import os
import subprocess

from django.core.management import call_command
from SEHub import settings

from SEHub.installed_urls import INSTALLED_URLS
from SEHub.installed_apps import INSTALLED_APPS
from SEHub.settings import SUDO_PASSWORD

INSTALLED_URLS_FILEPATH = 'SEHub/installed_urls.py'
INSTALLED_APPS_FILEPATH = 'SEHub/installed_apps.py'


def write_to_installed_urls(urls):
    f = open(INSTALLED_URLS_FILEPATH, 'w')
    f.write("INSTALLED_URLS = [\n")

    for app in urls:
        f.write("{'path': '" + app['path'] + "', 'prefix': '" + app['prefix'] + "' },\n")

    f.write("]\n")
    f.close()

def add_installed_urls(new_url):
    temp_urls = INSTALLED_URLS

    exists = False
    for url in temp_urls:
        if url['prefix'] == new_url['prefix']:
            exists = True

    if exists == False:
        temp_urls.append(new_url)
        write_to_installed_urls(temp_urls)

def remove_installed_urls(new_url):
    temp_urls = []

    for url in INSTALLED_URLS:
        if url['prefix'] != new_url['prefix']:
            temp_urls.append(url)

    write_to_installed_urls(temp_urls)

def write_to_installed_apps(apps):
    f = open(INSTALLED_APPS_FILEPATH, 'w')
    f.write("INSTALLED_APPS = (\n")

    for app in apps:
        f.write("'" + app + "',\n")

    f.write(")\n")
    f.close()

def add_installed_apps(new_app):
    if new_app not in INSTALLED_APPS:
        temp_apps = INSTALLED_APPS + (new_app,)
        write_to_installed_apps(temp_apps)

def remove_installed_apps(new_app):
    temp_apps = tuple(y for y in INSTALLED_APPS if y != new_app)
    write_to_installed_apps(temp_apps)

def pip_install(new_app):
    command = 'pip install %s' % new_app
    p = os.system('echo %s | sudo -S %s' % (SUDO_PASSWORD, command))

def pip_uninstall(new_app):
    command = 'pip uninstall -y %s' % new_app
    p = os.system('echo %s | sudo -S %s' % (SUDO_PASSWORD, command))

def install(package_name, app_name, prefix):
    pip_install(package_name)
    add_installed_apps(app_name)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SEHub.settings")
    call_command('syncdb', interactive=False)
    add_installed_urls({'path': app_name+'.urls', 'prefix':prefix})


def uninstall(package_name, app_name, prefix):
    pip_uninstall(package_name)
    remove_installed_apps(app_name)
    remove_installed_urls({'path': app_name+'.urls', 'prefix':prefix})
