from rest_framework import serializers
from AppManager.models import App


class AppSerializer(serializers.ModelSerializer):

    class Meta:
        model = App
        fields = ('id', 'name', 'package_name', 'desc', 'prefix', 'version')
