import os
import socket
import fcntl
import struct

from bluetooth import BluetoothSocket, RFCOMM, PORT_ANY, SERIAL_PORT_CLASS, SERIAL_PORT_PROFILE, OBEX_UUID
from bluetooth import discover_devices, lookup_name, advertise_service

from SEHub.settings import UUID


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


def get_nearby_services():
    target_port = None
    target_address = None

    print "searching for nearby devices.."
    nearby_devices = discover_devices()

    print "found %s devices" % len(nearby_devices)

    discovered_devices = []
    for addr in nearby_devices:
        temp = {}
        temp['name'] = lookup_name(addr)
        temp['addr'] = addr
        discovered_devices.append(temp)

        print "%s - %s" % (temp['name'], temp['addr'])

    return discovered_devices

def start_server():

    server_socket = BluetoothSocket(RFCOMM)
    server_socket.bind(("", PORT_ANY))
    server_socket.listen(1)

    port = server_socket.getsockname()[1]


    advertise_service(server_socket, "SeHub",
                      service_id = '2da22fce-0cb8-11e4-be42-0016eac5afde',
                      service_classes = [UUID, SERIAL_PORT_CLASS],
                      profiles = [SERIAL_PORT_PROFILE],)

    print "Waiting for connection on RFCOMM channel %d" % port

    while True:

        client_socket, client_info = server_socket.accept()

        print "Accepted connection from ", client_info

        try:
            data = client_socket.recv(1024)
            print "received: " + data
            client_socket.send("http://"+get_ip_address("wlan0")+":4000/")

        except IOError:
            pass

        client_socket.close()

    server_socket.close()
    print "Done"