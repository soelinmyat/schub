from django.contrib import admin

from ClassroomManager.models import Classroom, Classtime, Student, Enrollment


class ClasstimeInline(admin.TabularInline):
    model = Classtime


class ClassroomAdmin(admin.ModelAdmin):
    model = Classroom
    inlines = [ClasstimeInline]


admin.site.register(Classroom, ClassroomAdmin)
admin.site.register(Student)
admin.site.register(Enrollment)