from string import digits, letters
from random import choice

from django.db import models
from django.contrib.auth.models import User
from django_roa import Model as ROAModel

from SEHub.settings import SECENTRAL_IP


class CommonROAModel(ROAModel):

    class Meta:
        abstract = True

    @classmethod
    def get_resource_url_list(cls):
        temp_url = SECENTRAL_IP + "users/api/{base_name}/"
        return temp_url.format(
            base_name=cls.api_base_name,
        )

    def get_resource_url_count(self):
        return self.get_resource_url_list()


def random_string(length):
    s = ''
    for i in range(length):
        s += choice(digits+letters)

    return s


class Classroom(models.Model):
    name = models.CharField(max_length=255)
    secret = models.CharField(max_length=6, default=random_string(6))

    def __str__(self):
        return self.name


class Classtime(models.Model):

    DAY_CHOICES = (
        (0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday'),
    )

    DAY_CHOICES_ARRAY = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

    day = models.IntegerField(choices=DAY_CHOICES)
    starttime = models.TimeField()
    endtime = models.TimeField()
    name = models.CharField(max_length=255)
    classroom = models.ForeignKey(Classroom)

    def __str__(self):
        return self.classroom.name + ": " + self.DAY_CHOICES_ARRAY[self.day] + " " + self.starttime.strftime('%H:%M') + "-" + self.endtime.strftime('%H:%M')


class Student(CommonROAModel):
    id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=64)
    user = models.OneToOneField(User)
    passkey = models.CharField(max_length=32)

    api_base_name = ''

    @classmethod
    def serializer(cls):
        from .serializers import StudentSerializer
        return StudentSerializer

    def set_passkey(self):
        self.passkey = random_string(32)

    def __str__(self):
        return self.username


class Enrollment(models.Model):
    classroom = models.ForeignKey(Classroom)
    student = models.ForeignKey(Student)

    def __str__(self):
        return self.classroom.name + "-" + self.student.username