from rest_framework import serializers
from ClassroomManager.models import Student


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ('id', 'username', 'passkey')
