from django.conf.urls import patterns, include, url

from ClassroomManager import views

urlpatterns = patterns('',
    url(r'^register/$', views.register, name='register'),
)
