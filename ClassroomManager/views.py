import json
from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from ClassroomManager.models import Student, Classroom


#@require_http_methods(["POST"])
@csrf_exempt
def register(request):

    if request.method == 'POST':

        name = request.POST['name']
        email = request.POST['email']
        addr = request.POST['addr']
        c_id = request.POST['c_id']
        c_secret = request.POST['c_secret']

        temp_class = Classroom.objects.get(id=c_id, secret=c_secret)
        if temp_class:
            new_student=Student.objects.get_or_create(name=name, email=email, classroom=temp_class)
            new_student.addr = addr
            new_student.save()

            return HttpResponse(json.dumps({'message': 'success'}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({'message': 'error'}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'message': 'error'}), content_type="application/json")
