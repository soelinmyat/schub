from django.db import models

from ClassroomManager.models import Classroom, Classtime

# Create your models here.
class Poll(models.Model):
    question = models.TextField()
    option_1 = models.CharField(max_length=255)
    option_2 = models.CharField(max_length=255)
    option_3 = models.CharField(max_length=255)
    option_4 = models.CharField(max_length=255)
    classtime = models.ForeignKey(Classtime)
