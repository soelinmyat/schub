import kronos
import requests
import socket
import fcntl
import struct

from SEHub.settings import HUB_ID, SECENTRAL_IP

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


@kronos.register('* * * * *')
def update_status():
    url = SECENTRAL_IP+'hubs/status/'
    ip = get_ip_address('wlan0') #to be updated to wlan0

    data = {
        'hub_id': HUB_ID,
        'ip': "http://"+ip+":4000/"
    }

    requests.post(url,data=data)

    print data