from django.conf.urls import patterns, include, url
from django.contrib import admin

from installed_urls import INSTALLED_URLS


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SEHub.views.home', name='home'),
    url(r'^app/', include('AppManager.urls')),

    url(r'^', include(admin.site.urls)),
    #url('attendance/', include('SESmartAttendance.urls')),
    #url(r'^classrooms/', include('ClassroomManager.urls')),
    #url(r'^empty/', include('SEEmpty.urls')),
)

for app in INSTALLED_URLS:
   urlpatterns += patterns('', (app['prefix'], include(app['path'])))