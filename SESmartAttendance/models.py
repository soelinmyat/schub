from django.db import models

from ClassroomManager.models import Classtime, Student


class Attendance(models.Model):
    student = models.ForeignKey(Student)
    classtime = models.ForeignKey(Classtime)
    added = models.DateTimeField(auto_created=True)

    def __str__(self):
        return self.classtime.__str__() + "-" + self.student.username


