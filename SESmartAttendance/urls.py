from django.conf.urls import patterns, include, url

from SESmartAttendance import views


urlpatterns = patterns('',
    url(r'^take/$', views.take_attendance, name='take_attendance'),
)
