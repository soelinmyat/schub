import json
from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

# Create your views here.
from ClassroomManager.models import Classroom, Classtime, Enrollment, Student
from SESmartAttendance.models import Attendance

@csrf_exempt
def take_attendance(request):

    if request.method == 'POST':

        username = request.POST['username']
        passkey = request.POST['passkey']

        try:
            current_classtime = Classtime.objects.get(starttime__lte=datetime.now, endtime__gte=datetime.now)
            current_student = Student.objects.get(username=username)
            if current_classtime:
                is_enrolled = Enrollment.objects.get(classroom=current_classtime.classroom, student=current_student)
                if is_enrolled:
                    try:
                        new_attendance = Attendance.objects.get(classtime=current_classtime, student=current_student)
                        return HttpResponse(json.dumps({'message': 'attendance already taken at ' + current_classtime.classroom.name}), content_type="application/json")

                    except Attendance.DoesNotExist:
                        new_attendance = Attendance.objects.create(classtime=current_classtime, student=current_student, added=datetime.now())
                        new_attendance.save()
                        return HttpResponse(json.dumps({'message': 'attendance taken for' + current_classtime.classroom.name}), content_type="application/json")
                else:
                    return HttpResponse(json.dumps({'message': 'error'}), content_type="application/json")

            else:
                return HttpResponse(json.dumps({'message': 'no class'}), content_type="application/json")
        except:
            return HttpResponse(json.dumps({'message': 'no class'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': 'error'}), content_type="application/json")